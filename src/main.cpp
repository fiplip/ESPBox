/*******************************
* Project: ESPBox
* Filename: main.cpp
* 
* author: Filip Silverplats
* created: 2019-11-21
* notes:
* 
* desc: A simple game console that plays Snake on a Lolin D1 mini.
* 
* ver: 2019-11-21 first version
* 
* ******************************/

#include <Arduino.h>         // Arduino framework is used
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for display
#include <SPI.h>

#define TFT_RST -1 //for TFT I2C Connector Shield V1.0.0 and TFT 1.4 Shield V1.0.0
#define TFT_CS D4  //for TFT I2C Connector Shield V1.0.0 and TFT 1.4 Shield V1.0.0
#define TFT_DC D3  //for TFT I2C Connector Shield V1.0.0 and TFT 1.4 Shield V1.0.0
#define greenButton D2
#define redButton D1
#define buzzer D5

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

void titleScreen();
void playGame();
void resetGame();
void drawGame();
void playerInput();
void updateGame();
void gameOver();

int buttonStateGreen = 0;
int buttonStateRed = 0;

int score = 0;
int snakeX = 64;
int snakeY = 64;
int fruitX = 0;
int fruitY = 0;

bool fruitEaten = false;
bool gameOverState = false;
bool fruitOne = true;

enum Direction {LEFT, UP, RIGHT, DOWN};

class Snake
{
  public:
    //int snakeHeadX = 0; //Not used. Still is using old var that is not in class.
    //int snakeHeadY = 0; //Not used. Still is using old var that is not in class.
    int prevSnakeHeadX = 0;
    int prevSnakeHeadY = 0;
    int snakeTailX [30];
    int snakeTailY [30];
    int snakeLenght = 0;
    int prevTailX;
    int prevTailY;
    int prevTailXtemp;
    int prevTailYtemp;
    int snakeDir = 0;
};

Snake snake1;

void setup(void)
{
  pinMode(redButton, INPUT);
  pinMode(greenButton, INPUT);
  pinMode(buzzer, OUTPUT);

  tft.initR(INITR_144GREENTAB);
  tft.setTextWrap(false); // Allow text to run off right edge
  tft.fillScreen(ST7735_BLACK);

  Serial.begin(9600);
}

//Main loop
void loop(void)
{
  titleScreen();
  playGame();
  gameOver();
}

void titleScreen()
{
  tft.fillScreen(ST7735_BLACK);

  //Snake dropdown animation
  for(int i = 0; i < 45; i++)
  {
    tft.setCursor(20, i);
    tft.setTextColor(ST7735_GREEN);
    tft.setTextSize(3);
    tft.println("SNAKE");
    delay(5);
  }

  tft.fillScreen(ST7735_BLACK);
    
  tft.setCursor(20, 45);
  tft.setTextColor(ST7735_GREEN);
  tft.setTextSize(3);
  tft.println("SNAKE");

  tft.setCursor(58, 70);
  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(1);
  tft.println("By");

  tft.setCursor(15, 81);
  tft.println("Filip Silverplats");

  //Loop that keeps listening for buttonpress
  while(true)
  {
    buttonStateGreen = digitalRead(greenButton);
    buttonStateRed = digitalRead(redButton);

    if(buttonStateGreen == HIGH || buttonStateRed == HIGH)
    {
      break;
    }

    delay(20);
  }
}

void playGame()
{
  //Show story and choose snake. NOT IMPLEMENTED

  //Reset all the game variables
  resetGame();

  tft.fillScreen(ST7735_BLACK);

  while(gameOverState == false)
  {
    updateGame();
    drawGame();
    playerInput();
    delay(500);
  }
}

void resetGame()
{
  score = 0;
  //fruitX = random(1, 127);
  //fruitY = random(9, 127);
  fruitX = 88;
  fruitY = 56;
  snakeX = 64;
  snakeY = 64;
  gameOverState = false;
  fruitOne = true;
  fruitEaten = false;
  snake1.snakeDir = UP;
  snake1.snakeLenght = 3;
}

void drawGame()
{
  //Draw the HUD
  tft.setCursor(5, 0);
  tft.setTextColor(ST7735_WHITE);
  tft.println("Score = ");

  tft.setCursor(53, 0);
  tft.println(score);

  tft.drawRect(0, 8, 128, 120, ST7735_WHITE); //draw the box

  //Delete the old end of the tail.
  tft.fillCircle(snake1.prevTailX, snake1.prevTailY, 2, ST7735_BLACK);

  //Draw the Snake
  tft.fillCircle(snakeX, snakeY, 2, ST7735_GREEN);

  //Draw new fruit in a random location
  while(fruitEaten == true)
  {

    fruitX = random(1, 127);
    fruitY = random(9, 127);
    
    //Fruit must be placed on the grid on every 8 pixel.
    if(fruitX % 8 == 0 && fruitY % 8 == 0)
    {
      tft.fillCircle(fruitX, fruitY, 2, ST7735_RED);
      fruitEaten = false;
    }
  }
  while(fruitOne)
  {
    tft.fillCircle(fruitX, fruitY, 2, ST7735_RED);
    fruitOne = false;
  }
}

void playerInput()
{
  if(digitalRead(greenButton) == HIGH)
  {
    snake1.prevSnakeHeadX = snakeX;

    if(snake1.snakeDir == 0)
    {
      snake1.snakeDir = 3;
    }
    else
    {
      snake1.snakeDir--;
    }
  }
  else if(digitalRead(redButton) == HIGH)
  {
    snake1.prevSnakeHeadX = snakeX;

    if(snake1.snakeDir == 3)
    {
      snake1.snakeDir = 0;
    }
    else
    {
      snake1.snakeDir++;
    }
  }

}

void updateGame()
{
  snake1.prevTailX = snakeX;
  snake1.prevTailY = snakeY;
  //Keep track of the tail and save in arrays.
  for(int i = 0; i < snake1.snakeLenght; i++)
  {
    //spara gamla värdet från array först
    snake1.prevTailXtemp = snake1.snakeTailX[i];
    snake1.prevTailYtemp = snake1.snakeTailY[i];
    //skriv nya fräscha värden
    snake1.snakeTailX[i] = snake1.prevTailX;
    snake1.snakeTailY[i] = snake1.prevTailY;
    //skriv in gamla värdet och förbered för nästa loop.
    snake1.prevTailX = snake1.prevTailXtemp;
    snake1.prevTailY = snake1.prevTailYtemp;
  }

  //If the snake head is on the fruit
  if(snakeX == fruitX && snakeY == fruitY)
  {
    tft.setCursor(53, 0);
    tft.setTextColor(ST7735_BLACK);
    tft.println(score);
    score += 10;
    fruitEaten = true;
    snake1.snakeLenght++; //make snake longer
  }
  snake1.prevSnakeHeadX = snakeX;
  snake1.prevSnakeHeadY = snakeY;

  //Act on player input and change the direction of the snake.
  switch(snake1.snakeDir)
  {
    case LEFT:
      snakeX = snakeX - 8;
      break;
    case UP:
      snakeY = snakeY - 8;
      break;
    case RIGHT:
      snakeX = snakeX + 8;
      break;
    case DOWN:
      snakeY = snakeY + 8;
      break;
  }

  //Game over!
  if(snakeX == 0 || snakeY == 8 || snakeX == 120 || snakeY == 120)
  {
    gameOverState = true;
  }
}

void gameOver()
{
  //This is the death animation
  for(int i = 0; i < 3000; i++)
  {
    tft.fillCircle(random(1, 128), random(1, 128), 2, ST7735_RED);
  }

  tft.fillScreen(ST7735_BLACK);
  tft.setCursor(10, 45);
  tft.setTextColor(ST7735_RED);
  tft.setTextSize(2);
  tft.println("Game Over");

  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(1);
  tft.setCursor(25, 81);
  tft.println("Score = ");

  tft.setCursor(74, 81);
  tft.println(score); 
  
  while(true)
  {
    buttonStateGreen = digitalRead(greenButton);
    buttonStateRed = digitalRead(redButton);

    if(buttonStateGreen == HIGH || buttonStateRed == HIGH)
    {
      break;
    }

    delay(20);
  }
}